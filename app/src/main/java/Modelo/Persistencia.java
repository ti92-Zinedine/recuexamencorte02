package Modelo;

import com.example.recuexamenu2.IMCs;

public interface Persistencia {

    public void openDataBase();
    public void closeDataBase();
    public long insertProducto(IMCs imcs);
    public long updateProducto(IMCs imcs);
    public void deleteProducto(int id);

}
