package Modelo;

public class DefineTabla {

    public DefineTabla(){}

    public static abstract class Historial{
        // Variables necesarias
        public static final String TABLE_NAME = "historial";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_ALTURA = "altura";
        public static final String COLUMN_NAME_PESO = "peso";
        public static final String COLUMN_NAME_IMC = "imc";
    }

    // Creación de objeto para los resgistros
    public static String[] REGISTROS = new String[]{
            Historial.COLUMN_NAME_ID,
            Historial.COLUMN_NAME_ALTURA,
            Historial.COLUMN_NAME_PESO,
            Historial.COLUMN_NAME_IMC
    };
}
