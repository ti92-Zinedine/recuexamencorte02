package Modelo;

import android.database.Cursor;

import com.example.recuexamenu2.IMCs;

import java.util.ArrayList;

public interface Proyeccion {
    public IMCs getIMC(String id);
    public ArrayList<IMCs> allIMCs();
    public IMCs readIMC(Cursor cursor);
}
