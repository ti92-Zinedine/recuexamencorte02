package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.recuexamenu2.IMCs;

import java.util.ArrayList;

public class IMCsDb {

    private Context context;
    private IMCsDbHelper helper;
    private SQLiteDatabase db;

    public IMCsDb(Context context, IMCsDbHelper helper){
        this.context=context;
        this.helper=helper;
    }

    public IMCsDb(Context context){
        this.context=context;
        this.helper=new IMCsDbHelper(this.context);
    }


    @Override
    public void openDataBase() {
        db=helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    public long insertIMC(IMCs imc) {

        ContentValues values=new ContentValues();

        values.put(DefineTabla.Historial.COLUMN_NAME_ALTURA, imc.getAltura());
        values.put(DefineTabla.Historial.COLUMN_NAME_PESO, imc.getPeso());
        values.put(DefineTabla.Historial.COLUMN_NAME_IMC,imc.getImc());

        this.openDataBase();
        long num = db.insert(DefineTabla.Historial.TABLE_NAME, null, values);
        this.closeDataBase();
        Log.d("agregar","insertIMC: "+num);

        return num;
    }

    @Override
    public long updateIMC(IMCs imc) {
        ContentValues values=new ContentValues();

        values.put(DefineTabla.Historial.COLUMN_NAME_ALTURA, imc.getAltura());
        values.put(DefineTabla.Historial.COLUMN_NAME_PESO, imc.getPeso());
        values.put(DefineTabla.Historial.COLUMN_NAME_IMC,imc.getImc());

        this.openDataBase();
        long num = db.update(
                DefineTabla.Historial.TABLE_NAME,
                values,
                DefineTabla.Historial.COLUMN_NAME_ID+"="+imc.getId(),
                null);
        this.closeDataBase();
        Log.d("agregar","insertVenta: "+num);

        return num;
    }

    @Override
    public void deleteIMCs(int id) {
        this.openDataBase();
        db.delete(
                DefineTabla.Historial.TABLE_NAME,
                DefineTabla.Historial.COLUMN_NAME_ID+"=?",
                new String[] {String.valueOf(id)});
        this.closeDataBase();
    }


    @Override
    public IMCs getIMC(String id) {
        db = helper.getWritableDatabase();

        String stringId = String.valueOf(id);

        Cursor cursor = db.query(
                DefineTabla.Historial.TABLE_NAME,
                DefineTabla.REGISTROS,
                DefineTabla.Historial.COLUMN_NAME_ID + " = ?",
                new String[]{stringId},
                null, null, null);

        if (cursor.moveToFirst()) {
            IMCs imc = readIMC(cursor);
            cursor.close();
            return imc;
        } else {
            cursor.close();
            return null; // No se encontró en la base de datos
        }
    }

    @Override
    public ArrayList<IMCs> allIMCs() {
        db=helper.getWritableDatabase();

        Cursor cursor=db.query(
                DefineTabla.Historial.TABLE_NAME,
                DefineTabla.REGISTROS,
                null, null, null, null, null);
        ArrayList<IMCs> imcs = new ArrayList<IMCs>();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            IMCs imc=readIMC(cursor);
            imcs.add(imc);
            cursor.moveToNext();
        }

        cursor.close();
        return imcs;
    }

    @Override
    public IMCs readIMC(Cursor cursor) {
        IMCs imc=new IMCs();

        imc.setId(cursor.getInt(0));
        imc.setAltura(cursor.getFloat(1));
        imc.setPeso(cursor.getFloat(2));
        imc.setImc(cursor.getFloat(3));

        return imc;
    }
}
