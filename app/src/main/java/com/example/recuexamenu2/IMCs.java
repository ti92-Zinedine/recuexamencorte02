package com.example.recuexamenu2;

public class IMCs {
    // Atributos
    private int id;
    private float altura;
    private float peso;
    private float imc;

    // Constructor
    public IMCs() {
        this.id = 0;
        this.altura = 0.0F;
        this.peso = 0.0F;
        this.imc = 0.0F;
    }

    public IMCs(int id, float altura, float peso, float imc) {
        this.id = id;
        this.altura = altura;
        this.peso = peso;
        this.imc = imc;
    }

    // Encapsulamiento

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float getImc() {
        return imc;
    }

    public void setImc(float imc) {
        this.imc = imc;
    }
}
